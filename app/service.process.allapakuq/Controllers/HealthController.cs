﻿using Microsoft.AspNetCore.Mvc;

namespace service.process.allapakuq.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        public HealthController()
        {

        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok("Ok");
        }
    }
}