﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using service.process.allapakuq.Application.Commands.UserCommand;
using service.process.allapakuq.Infraestructure.Common;
using System.Collections.Generic;
using Twilio;
using Twilio.Rest.Notify.V1.Service;

namespace service.process.allapakuq.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public readonly TwilioSecret _twilioSecret;
        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
            _twilioSecret = new TwilioSecret();
            configuration.GetSection("TwilioSecret").Bind(_twilioSecret);
            TwilioClient.Init(_twilioSecret.AccountSID, _twilioSecret.AuthToken);
        }

        [HttpGet]
        [Route("Login")]
        public ActionResult Get()
        {
            return Ok("Ok");
        }

        [HttpPost]
        [Route("PhoneVerification")]
        public OperationResult<string> PostPhoneVerification([FromBody] PhoneValidationCommand command)
        {
            // Find your Account SID and Auth Token at twilio.com/console
            var code = Util.RandomString(6);
            var notification = NotificationResource.Create(
                _twilioSecret.ServiceSID,
                toBinding: new List<string> { "{\"binding_type\":\"sms\",\"address\":\""+ command.CellPhoneNumber + "\"}" },
                body: $"Your Allapakuq verification code is: {code}");

            return new OperationResult<string>(notification.Sid);
        }
    }
}