﻿using System;
using System.Collections.Generic;
using System.Text;

namespace service.process.allapakuq.Infraestructure.Common
{
    public class OperationResult : Object
    {
        public bool isValid { get; set; } = false;
        public List<OperationException> exceptions { get; set; } = new List<OperationException>();
    }
    public class OperationResult<T> : OperationResult
    {
        public T content { get; set; } = default(T);

        public OperationResult(T obj)
        {
            isValid = true;
            content = obj;
        }

    }

    public class OperationException : Object
    {
        public string code { get; set; }
        public string description { get; set; }
        public OperationException(string code, string description) : base()
        {
            this.code = code;
            this.description = description;
        }
        public OperationException()
        {

        }
    }
}
