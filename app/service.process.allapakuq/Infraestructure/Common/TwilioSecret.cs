﻿namespace service.process.allapakuq.Infraestructure.Common
{
    public class TwilioSecret
    {
        public string AccountSID { get; set; }
        public string AuthToken { get; set; }
        public string ServiceSID { get; set; }
    }
}
