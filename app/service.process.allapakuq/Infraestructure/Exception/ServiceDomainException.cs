﻿using System;

namespace service.process.allapakuq.Infraestructure.Exceptions
{
    public class ServiceDomainException : Exception
    {
        public ServiceDomainException()
        { }

        public ServiceDomainException(string message)
            : base(message)
        { }

        public ServiceDomainException(string message, Exception innerException)
           : base(message, innerException)
        { }

    }
}
