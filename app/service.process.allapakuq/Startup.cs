using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using service.process.allapakuq.Infraestructure.Middleware;

namespace service.process.allapakuq
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IConfiguration _configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCustomMvc()
                .AddCustomAuthentication(_configuration)
                //.AddCustomHttp()
                .AddLogging(config =>
                {
                    config.SetMinimumLevel(LogLevel.Debug);
                })
                .AddCustomSwagger();

            services.AddSingleton(_configuration);

            //var builder = new ContainerBuilder();
            //builder.Populate(services);
            //builder.RegisterModule(new CommandModule());
            //builder.RegisterModule(new MediatorModule());
            //builder.RegisterModule(new ServiceModule(_configuration));
            //builder.RegisterModule(new RepositoryModule(_configuration));

            //return new AutofacServiceProvider(builder.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            app.UseSwagger()
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Allapakuq.API V1");
            });

            app.UseAuthentication();

            app.UseCors("CorsPolicy");

            //app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
