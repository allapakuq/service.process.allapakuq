<p align="center">
  <a href="https://docs.microsoft.com/es-es/dotnet/">
    <img src="https://seeklogo.com/images/C/c-sharp-c-logo-02F17714BA-seeklogo.com.png" alt="Logo" width=72 height=72>
  </a>

  <h3 align="center">Process Service :globe_with_meridians: :robot: :lock:</h3>

  <p align="center">
    This centralized service for the allapakuq application.
    <br>
    Base project made with much  :heart: . Contains jwt, identify, authorize, CQRS pattern, and much more!
    <br>
    <br>
    <a href="https://jhon-coronel-bautista.atlassian.net/jira/software/projects/AQ/boards/1">Jira board</a>
	·
    <a href="https://bitbucket.org/allapakuq/app.mobile.allapakuq/jira">Jira issues</a>
  </p>
</p>

## Table of contents

- [Getting Started](#getting-started)
- [Copyright and license](#copyright-and-license)

## Getting Started

The repository code is preloaded with some basic components like basic app architecture, app theme, constants and required dependencies to create a new project.

## How to Use 

**Step 1:**

Download or clone this repo by using the link below:

```
git clone https://Jhon-cb@bitbucket.org/allapakuq/service.process.allapakuq.git
```

**Step 2:**

Run commands in the following path: 

```
cd app
```

## Copyright and License
 
The MIT License (MIT)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Copyright (c) 2022 Allapakuq

Enjoy :gem:
